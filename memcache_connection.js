const { Client } = require("memjs");

const memcached = Client.create();

module.exports = memcached;
